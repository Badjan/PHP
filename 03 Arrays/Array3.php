﻿<?php
$file = fopen('demo_products.txt', 'r');
$str = fgets($file);
$str = fgets($file);
$gardencenter = [];
while (!feof($file))
{
    $parts = explode("\t", $str);
    $gardencenter[$parts[0]] = [
        "name_en"   => $parts[1],
        "name_ua"   => $parts[2],
        "category"  => $parts[3],
        "price"     => $parts[4],
    ];
    $str = fgets($file);
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Array task3</title>
  <meta charset="UTF-8">
  </head>
<body>
<table>
        <?php foreach ($gardencenter as $key => $value) : ?>
           <tr><td> <?= $key; ?> </td>
           <td> <?= $value["name_en"]; ?> </td>
           <td> <?= $value["name_ua"]; ?> </td>
           <td> <?= $value["category"]; ?> </td>
           <td> <?= $value["price"]; ?> </td></tr>
        <?php endforeach; ?>
        </table>
 </body>
</html>