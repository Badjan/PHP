<?php
$months = [
    1   =>  [
        "short" => "Jan",
        "long"  => "January",
    ],
    2   =>  [
        "short" => "Feb",
        "long"  => "February",
    ],
    3   =>  [
        "short" => "Mar",
        "long"  => "March",
    ],
    4   =>  [
        "short" => "Apr",
        "long"  => "April",
    ],
    5   =>  [
        "short" => "May",
        "long"  => "May",
    ],
    6   =>  [
        "short" => "Jun",
        "long"  => "June",
    ],
    7   =>  [
        "short" => "Jul",
        "long"  => "July",
    ],
    8   =>  [
        "short" => "Aug",
        "long"  => "August",
    ],
    9   =>  [
        "short" => "Sep",
        "long"  => "September",
    ],
    10   =>  [
        "short" => "Oct",
        "long"  => "October",
    ],
    11   =>  [
        "short" => "Nov",
        "long"  => "November",
    ],
    12   =>  [
        "short" => "Dec",
        "long"  => "December",
    ],
];
 ?>       
        <table>
        <?php foreach ($months as $key => $month) : ?>
           <tr><td> <?= $key; ?> </td>
           <td> <?= $month["short"]; ?> </td>
           <td> <?= $month["long"]; ?> </td></tr>
        <?php endforeach; ?>
        </table>

