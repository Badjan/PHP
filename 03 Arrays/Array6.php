<?php
$users2 = [
    0 => [
        "name" => "Alex",
        "lastname" => "Petrenko",
        "languages" => array("Java", "Python", "C++")
    ],
    1 => [
        "name" => "Boris",
        "lastname" => "Vasylenko",
        "languages" => array("PHP", "Ruby", "C++")
    ],
    2 => [
        "name" => "Vasiliy",
        "lastname" => "Alibabayev",
        "languages" => array("C#", "JavaScript", "HTML", "PHP")
    ],
];

$users_name = array();
    foreach($users2 as $key=>$user){
      $users_name[$key] = $user["lastname"];
    }

    $users_name_sorted = $users2;
    array_multisort($users_name, SORT_REGULAR, $users_name_sorted);
  ?>
  <ul>
        <?php
            foreach($users_name_sorted as $key => $user){
                echo "<li>", $user["name"], " ", $user["lastname"], "<ul>";
                foreach($user["languages"] as $key1 => $value){
                    echo "<li>{$value}</li>";
                }
                echo "</ul>","</li>";
            }
        ?>
  </ul>