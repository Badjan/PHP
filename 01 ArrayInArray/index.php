<?php
  function concat ($a = "1", $b = "2"){
    $c = $a." ".$b;
    return $c;
  }

  function hello ($firstname = "Name", $lastname = "Lastname"){
    $fullName = $firstname." ".$lastname;
    return $fullName;
  }

  function debugOutput($myStr){
    echo "<pre>";
    print_r($myStr);
    echo "</pre>";
  }

  function output($someArray){
    
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Lab Two. Arrays</title>
</head>
<body>
  <?php
    echo "<h2>Lab. Arrays</h2>", "<br>";

    //TASk 1
    echo "<h3>Task 1</h3>", "<br>";

    echo concat("One", "Two"), "<br>", "<br>";

    $firstname = "Alex";
    $lastname = "Freeman";
    $hello = hello($firstname, $lastname);
    echo $hello, "<br>";
    echo hello("John", "Biggs"), "<br>";
    echo hello(), "<br>";


    //TASk 2
    echo "<h3>Task 2</h3>", "<br>";

    $arr = array("Firstname" => "John", "Lastname" => "Silver", "Occupation" => "Pirate");
    debugOutput($arr);
    echo "<br>";


    //TASk 3
    echo "<h3>Task 3</h3>", "<br>";

    $users = array(
      0 => array(
          "name" => "Alex",
          "lastname" => "Petrenko",
          "age" => 14
      ),
      1 => array(
          "name" => "Boris",
          "lastname" => "Vasylenko",
          "age" => 13
      ),
      2 => array(
          "name" => "Vasiliy",
          "lastname" => "Alibabayev",
          "age" => 12
      )
    );

    $users_age = array();
    foreach($users as $key=>$user){
    $users_age[$key] = $user["age"];
    }
    $users_sorted = $users;
    array_multisort($users_age, SORT_NUMERIC, $users_sorted);

    debugOutput($users_sorted);
    echo "<br>";

    $age = 0;
    foreach($users as $key=>$user){
      $age += $user["age"]; 
    }
    $average_age = $age / count($users);
    echo "{$age}<br>{$average_age}";


  ?>
  <table border = "1px">
        <tr>
            <td>FirstName</td>
            <td>LastName</td>
            <td>Age</td>
        </tr>
        <?php
            foreach($users_sorted as $key => $user){
                echo "<tr>";
                foreach($user as $key1 => $value){
                    echo "<td>{$value}</td>";
                }
                echo "</tr>";
            }
        ?>
    </table>
  <?php
    echo "<h3>Task 4</h3>", "<br>";

    $users2 = array(
    0 => array(
        "name" => "Alex",
        "lastname" => "Petrenko",
        "languages" => array("Java", "Python", "C++")
    ),
    1 => array(
        "name" => "Boris",
        "lastname" => "Vasylenko",
        "languages" => array("PHP", "Ruby", "C++")
    ),
    2 => array(
        "name" => "Vasiliy",
        "lastname" => "Alibabayev",
        "languages" => array("C#", "JavaScript", "HTML", "PHP")
    )
);

    $users_name = array();
    foreach($users2 as $key=>$user){
      $users_name[$key] = $user["lastname"];
    }

    $users_name_sorted = $users2;
    array_multisort($users_name, SORT_REGULAR, $users_name_sorted);
  ?>
  <ul>
        <?php
            foreach($users_name_sorted as $key => $user){
                echo "<li>", $user["name"], " ", $user["lastname"], "<ul>";
                foreach($user["languages"] as $key1 => $value){
                    echo "<li>{$value}</li>";
                }
                echo "</ul>","</li>";
            }
        ?>
  </ul>

</body>
</html>